import React, { useContext, Fragment } from 'react'
import { Segment, Header, Button, Container } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import { RootStoreContext } from '../../app/stores/rootStore';
  
const UserInfo= () => {
    const rootStore = useContext(RootStoreContext);
    const { user,isLoggedIn  } = rootStore.userStore;
    return (
      <Segment inverted textAlign='center' vertical className='masthead'>
      <Container text>
        <Header as='h1' inverted>
        </Header>
            {isLoggedIn && user ? (
          <Fragment>
            <Header as='h2' inverted content={`Pseudo : ${user.displayName} `} color='blue'/>
            <Header as='h2' inverted content={`Nom : ${user.username} `}   color='blue'/>
            <Header as='h2' inverted content={`Image :  ${user.image} `} color='blue'/>
            <Button as={Link} to='/activities' size='huge' inverted color='blue'>
              Retourner a la page d'activités
            </Button>
          </Fragment>
        ) : (
          <Fragment>
          <Header as='h2' inverted content={`Projet EHEI 2020 `} />
        </Fragment>
        )}
              </Container>
    </Segment>
        
    );
}

export default UserInfo
