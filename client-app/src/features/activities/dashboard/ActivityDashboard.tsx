import React, { useContext, useEffect } from 'react';
import { Grid, Advertisement } from 'semantic-ui-react';
import ActivityList from './ActivityList';
import { observer } from 'mobx-react-lite';
import LoadingComponent from '../../../app/layout/LoadingComponent';
import { RootStoreContext } from '../../../app/stores/rootStore';

const AdvertisementExampleCommonUnits = () => (
  <div>
    <Advertisement unit='half page' test='Publicité' src='google.com'/>
    <Advertisement unit='medium rectangle' test='Publicité' />
  </div>
)

const ActivityDashboard: React.FC = () => {

  const rootStore = useContext(RootStoreContext);
  const {loadActivities, loadingInitial} = rootStore.activityStore;

  useEffect(() => {
    loadActivities(); 
  }, [loadActivities]);

  if (loadingInitial)
    return <LoadingComponent content='Loading activities' />;

  return (
    <Grid>
      <Grid.Column width={10}>
        <ActivityList />
      </Grid.Column>
      <Grid.Column width={6}>
        {AdvertisementExampleCommonUnits()}
      </Grid.Column>
    </Grid>
  );
};

export default observer(ActivityDashboard);
