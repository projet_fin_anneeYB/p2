import React, { Fragment } from 'react';
import { Segment, Header, Form, Button, Comment } from 'semantic-ui-react';

const ActivityDetailedChat = () => {
  return (
    <Fragment>
      <Segment
        textAlign='center'
        attached='top'
        inverted
        color='blue'
        style={{ border: 'none' }}
      >
        <Header>Discussion de l'evenement</Header>
      </Segment>
      <Segment attached>
        <Comment.Group>
          <Comment>
            <Comment.Avatar src='/assets/user.png' />
            <Comment.Content>
              <Comment.Author as='a'>Yahya</Comment.Author>
              <Comment.Metadata>
                <div>Today at 5:42PM</div>
              </Comment.Metadata>
              <Comment.Text>Salam !</Comment.Text>
              <Comment.Actions>
                <Comment.Action>Repondre</Comment.Action>
              </Comment.Actions>
            </Comment.Content>
          </Comment>

          <Comment>
            <Comment.Avatar src='/assets/user.png' />
            <Comment.Content>
              <Comment.Author as='a'>Rziki</Comment.Author>
              <Comment.Metadata>
                <div>5 days ago</div>
              </Comment.Metadata>
              <Comment.Text>je participe</Comment.Text>
              <Comment.Actions>
                <Comment.Action>Repondre</Comment.Action>
              </Comment.Actions>
            </Comment.Content>
          </Comment>

          <Form reply>
            <Form.TextArea />
            <Button
              content='Repondre'
              labelPosition='left'
              icon='edit'
              primary
            />
          </Form>
        </Comment.Group>
      </Segment>
    </Fragment>
  );
};

export default ActivityDetailedChat;
