import React, { useContext, Fragment } from 'react';
import { Container, Segment, Header, Button, Image } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import { RootStoreContext } from '../../app/stores/rootStore';
import LoginForm from '../user/LoginForm';
import RegisterForm from '../user/RegisterForm';

const HomePage = () => {
  const rootStore = useContext(RootStoreContext);
  const { user, isLoggedIn } = rootStore.userStore;
  const {openModal} = rootStore.modalStore;
  return (
    <Segment inverted textAlign='center' vertical className='masthead'>
      <Container text>
        <Header as='h1' inverted>
          <Image
            fluid
            src='/assets/logo.png'
            alt='logo'
            style={{ marginBottom: 12 }}
          />
        </Header>
        {isLoggedIn && user ? (
          <Fragment>
            <Header as='h2' inverted content={`Envie de nous quitter  ${user.displayName} ?`} />
            <Button as={Link} to='/activities' size='huge' inverted>
              Retourner a la page d'activités
            </Button>
          </Fragment>
        ) : (
          <Fragment>
          <Header as='h2' inverted content={`Projet EHEI 2020 `} />
          <Button onClick={() => openModal(<LoginForm />)} size='huge' inverted>
            Se connecter
          </Button>
          <Button onClick={() => openModal(<RegisterForm />)} size='huge' inverted>
            S'enregistrer
          </Button>
        </Fragment>
        )}
      </Container>
    </Segment>
  );
};

export default HomePage;
